<h1 align="center">
  Omid Baharifar (weblax)
</h1>

<h4 align="center">This is Omidi! A junior front end developer, also known as Weblax!</h4>

## My social networks
<a href="https://instagram.com/weblax_ir">
Instagram
</a>
<a href="https://www.youtube.com/channel/UC9l_5Xfj0KnQda1hkf_nSWQ">
YouTube
</a>
<a href="https://t.me/weblax_ir">
Telegram
</a>
